// Copyright 2023 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

#ifdef COOT_USE_OPENCL

TEMPLATE_TEST_CASE("mat_advanced_cl", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  if (coot::get_rt().backend != CL_BACKEND)
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(10, 10, distr_param(1, 10));
  cl_mem ptr = x.get_dev_mem(false).cl_mem_ptr.ptr;

  Mat<eT> y(ptr, 10, 10);

  REQUIRE( all(all(x == y)) );
  }

#endif



#ifdef COOT_USE_CUDA

TEMPLATE_TEST_CASE("mat_advanced_cuda", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  if (coot::get_rt().backend != CUDA_BACKEND)
    {
    return;
    }

  Mat<eT> x = randi<Mat<eT>>(10, 10, distr_param(1, 10));
  eT* ptr = x.get_dev_mem(false).cuda_mem_ptr;

  Mat<eT> y(ptr, 10, 10);

  REQUIRE( all(all(x == y)) );
  }

#endif



TEMPLATE_TEST_CASE("clear_empty_test", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x;
  x.randu(50, 100);

  x.clear();

  REQUIRE( x.n_rows == 0 );
  REQUIRE( x.n_cols == 0 );
  REQUIRE( x.empty() == true );
  }



TEMPLATE_TEST_CASE("size_test", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(15, 20);

  REQUIRE( x.size() == 300 );

  x.zeros(3, 6);

  REQUIRE( x.size() == 18 );

  x.clear();

  REQUIRE( x.size() == 0 );

  x.set_size(100, 0);

  REQUIRE( x.size() == 0 );
  }



TEMPLATE_TEST_CASE("front_back_test", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x;
  x.randu(75, 55);
  x.at(0, 0) = (eT) 3;
  x.at(74, 54) = (eT) 6;
  x.at(10, 10) = (eT) 4;
  x.at(64, 44) = (eT) 5;

  REQUIRE( x.front() == Approx((eT) 3) );
  REQUIRE( x.back()  == Approx((eT) 6) );

  REQUIRE( x.submat(10, 10, 64, 44).front() == Approx((eT) 4) );
  REQUIRE( x.submat(10, 10, 64, 44).back()  == Approx((eT) 5) );
  }



TEST_CASE("front_back_empty_test", "[mat]")
  {
  Mat<u32> m;

  REQUIRE_THROWS( m.front() );
  REQUIRE_THROWS( m.back() );
  }



TEMPLATE_TEST_CASE("subview_operators_test", "[mat]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(50, 50, fill::zeros);
  x(11, 11) = (eT) 5;
  subview<eT> y = x.submat(10, 10, 30, 30);
  Mat<eT> z = x.submat(10, 10, 30, 30);

  REQUIRE( ((eT) y[0])     == (eT) 0 );
  REQUIRE( ((eT) y[22])    == (eT) 5 );
  REQUIRE( ((eT) y.at(0))  == (eT) 0 );
  REQUIRE( ((eT) y.at(22)) == (eT) 5 );
  REQUIRE( ((eT) y(0))     == (eT) 0 );
  REQUIRE( ((eT) y(22))    == (eT) 5 );

  REQUIRE( ((eT) y(0, 0))    == (eT) 0 );
  REQUIRE( ((eT) y(1, 1))    == (eT) 5 );
  REQUIRE( ((eT) y.at(0, 0)) == (eT) 0 );
  REQUIRE( ((eT) y.at(1, 1)) == (eT) 5 );

  // Now set the values with the different versions.
  y[1] = (eT) 1;
  y(4) = (eT) 4;
  y.at(6) = (eT) 6;
  y(5, 7) = (eT) 30;
  y.at(8, 9) = (eT) 44;

  REQUIRE( ((eT) x.at(11, 10)) == (eT) 1  );
  REQUIRE( ((eT) x.at(14, 10)) == (eT) 4  );
  REQUIRE( ((eT) x.at(16, 10)) == (eT) 6  );
  REQUIRE( ((eT) x.at(15, 17)) == (eT) 30 );
  REQUIRE( ((eT) x.at(18, 19)) == (eT) 44 );
  }
