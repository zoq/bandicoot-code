// Copyright 2023 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

// Shuffle a 10-element vector and make sure we get all the elements back.
TEMPLATE_TEST_CASE("simple_shuffle_vec_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> v(10);
  for (uword i = 0; i < 10; ++i)
    {
    v[i] = (eT) i;
    }

  Col<eT> sv = shuffle(v);

  REQUIRE( sv.n_elem == v.n_elem );
  std::vector<bool> found(10, false);

  for (uword i = 0; i < 10; ++i)
    {
    found[(size_t) sv[i]] = true;
    }

  for (uword i = 0; i < 10; ++i)
    {
    REQUIRE(found[i] == true);
    }
  }



// Make sure sorting works when the size is a power of 2.
TEMPLATE_TEST_CASE("shuffle_vec_pow2_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 5; p < 13; ++p)
    {
    const size_t len = std::pow(2, p);
    Row<eT> r = linspace<Row<eT>>(0, len - 1, len);
    Row<eT> r2 = shuffle(r);

    REQUIRE( r.n_elem == r2.n_elem );

    std::vector<bool> found(r.n_elem, false);
    arma::Row<eT> r2_cpu(r2);
    for (uword i = 0; i < r2_cpu.n_elem; ++i)
      {
      found[(size_t) r2_cpu[i]] = true;
      }

    for (uword i = 0; i < r2_cpu.n_elem; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }


// Make sure sorting works when the size is one greater than a power of 2
// (this means that there is a lot of unused threads in the shuffle_vec kernel).
TEMPLATE_TEST_CASE("shuffle_vec_gt_pow2_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 5; p < 13; ++p)
    {
    const size_t len = std::pow(2, p) + 1;
    Row<eT> r = linspace<Row<eT>>(0, len - 1, len);

    Row<eT> r2 = shuffle(r);

    REQUIRE( r.n_elem == r2.n_elem );

    std::vector<bool> found(r.n_elem, false);
    arma::Row<eT> r2_cpu(r2);
    for (uword i = 0; i < r2_cpu.n_elem; ++i)
      {
      found[(size_t) r2_cpu[i]] = true;
      }

    for (uword i = 0; i < r2_cpu.n_elem; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }



// Shuffle a large vector, then make sure it can be re-sorted.
TEMPLATE_TEST_CASE("large_shuffle_vec_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c = linspace<Col<eT>>(0, 99999, 100000);
  Col<eT> c2 = shuffle(c);

  REQUIRE( c.n_elem == c2.n_elem );

  std::vector<bool> found(c.n_elem, false);
  arma::Col<eT> c2_cpu(c2);
  for (uword i = 0; i < c2_cpu.n_elem; ++i)
    {
    found[(size_t) c2_cpu[i]] = true;
    }

  for (uword i = 0; i < c2_cpu.n_elem; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Make sure a vector of floating-point values can be sorted.
TEMPLATE_TEST_CASE("large_fp_shuffle_test", "[shuffle]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c(100000, fill::randu);
  Col<eT> c2 = shuffle(c);

  REQUIRE( c2.n_elem == c.n_elem );

  Col<eT> c_sorted = sort(c);
  Col<eT> c2_sorted = sort(c2);

  REQUIRE( all(c_sorted == c2_sorted) );
  }



// Shuffle a vector in-place.
TEMPLATE_TEST_CASE("shuffle_vec_inplace_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c = linspace<Col<eT>>(0, 99, 100);
  c = shuffle(c);

  REQUIRE( c.n_elem == 100 );
  std::vector<bool> found(c.n_elem, false);
  arma::Col<eT> c_cpu(c);
  for (uword i = 0; i < 100; ++i)
    {
    found[(size_t) c_cpu[i]] = true;
    }

  for (uword i = 0; i < 100; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle an expression on a vector.
TEMPLATE_TEST_CASE("shuffle_vec_expr_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c = linspace<Col<eT>>(0, 99, 100);
  Col<eT> c2 = shuffle(2 * c + 1);

  REQUIRE( c.n_elem == c2.n_elem );
  std::vector<bool> found(c.n_elem, false);
  arma::Col<eT> c2_cpu(c2);
  for (uword i = 0; i < 100; ++i)
    {
    found[(size_t) ((c2_cpu[i] - 1) / 2)] = true;
    }

  for (uword i = 0; i < 100; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle the rows of a simple small matrix.
TEMPLATE_TEST_CASE("simple_shuffle_rowwise_mat_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(12, 10);
  for (uword i = 0; i < m.n_rows; ++i)
    m.row(i).fill(i);

  Mat<eT> m2 = shuffle(m, 0);

  REQUIRE( m2.n_rows == m.n_rows );
  REQUIRE( m2.n_cols == m.n_cols );

  std::vector<bool> found(m.n_rows, false);
  for (uword i = 0; i < m.n_rows; ++i)
    {
    const uword val = (uword) m2(i, 0);
    REQUIRE( val < m.n_rows );
    found[val] = true;

    for (uword j = 1; j < m2.n_cols; ++j)
      {
      REQUIRE( ((eT) m2(i, j)) == ((eT) m2(i, 0)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle the columns of a simple small matrix.
TEMPLATE_TEST_CASE("simple_shuffle_colwise_mat_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(10, 12);
  for (uword i = 0; i < m.n_cols; ++i)
    m.col(i).fill(i);

  Mat<eT> m2 = shuffle(m, 1);

  REQUIRE( m2.n_rows == m.n_rows );
  REQUIRE( m2.n_cols == m.n_cols );

  std::vector<bool> found(m.n_cols, false);
  for (uword i = 0; i < m.n_cols; ++i)
    {
    const uword val = (uword) m2(0, i);
    REQUIRE( val < m.n_cols );
    found[val] = true;

    for (uword j = 1; j < m2.n_rows; ++j)
      {
      REQUIRE( ((eT) m2(j, i)) == ((eT) m2(0, i)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle the rows of a one-column matrix of varying sizes.
TEMPLATE_TEST_CASE("shuffle_row_onecol_mat_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 7; p < 12; ++p)
    {
    const uword len = std::pow(2, p);
    Mat<eT> m(len, 1);
    m.col(0) = linspace<Col<eT>>(0, len - 1, len);
    Mat<eT> m2 = shuffle(m, 0);

    REQUIRE( m.n_rows == m2.n_rows );
    REQUIRE( m.n_cols == m2.n_cols );

    std::vector<bool> found(len, false);
    arma::Mat<eT> m2_cpu(m2);
    for (uword i = 0; i < m2_cpu.n_elem; ++i)
      {
      found[(uword) m2_cpu[i]] = true;
      }

    for (uword i = 0; i < m2_cpu.n_elem; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }



// Shuffle the columns of a one-row matrix of varying sizes.
TEMPLATE_TEST_CASE("shuffle_col_onerow_mat_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 7; p < 12; ++p)
    {
    const uword len = std::pow(2, p);
    Mat<eT> m(1, len);
    m.row(0) = linspace<Row<eT>>(0, len - 1, len);
    Mat<eT> m2 = shuffle(m, 1);

    REQUIRE( m.n_rows == m2.n_rows );
    REQUIRE( m.n_cols == m2.n_cols );

    std::vector<bool> found(len, false);
    arma::Mat<eT> m2_cpu(m2);
    for (uword i = 0; i < m2_cpu.n_elem; ++i)
      {
      found[(uword) m2_cpu[i]] = true;
      }

    for (uword i = 0; i < m2_cpu.n_elem; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }



// Shuffle the columns and rows of a matrix of varying sizes that are powers of 2.
TEMPLATE_TEST_CASE("shuffle_mat_pow2_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 8; p < 12; ++p)
    {
    const uword row_p = p - 5;
    const uword col_p = p + 1;
    const uword rows = std::pow(row_p, 2);
    const uword cols = std::pow(col_p, 2);
    arma::Mat<eT> m_cpu(rows, cols);
    for (uword i = 0; i < m_cpu.n_elem; ++i)
      {
      m_cpu[i] = i;
      }

    Mat<eT> m(m_cpu);
    Mat<eT> m2 = shuffle(m, 0);
    Mat<eT> m3 = shuffle(m, 1);

    REQUIRE( m.n_rows == m2.n_rows );
    REQUIRE( m.n_cols == m2.n_cols );
    REQUIRE( m.n_rows == m3.n_rows );
    REQUIRE( m.n_cols == m3.n_cols );

    // First check the one where we shuffled rows.
    std::vector<bool> found(m.n_rows, false);
    arma::Mat<eT> m2_cpu(m2);
    for (uword i = 0; i < m2_cpu.n_rows; ++i)
      {
      // Map the first element of the row back to its original row.
      const uword orig_row = (uword) m2_cpu(i, 0);
      found[orig_row] = true;

      for (uword j = 1; j < m2_cpu.n_cols; ++j)
        {
        REQUIRE( m_cpu(orig_row, j) == Approx(m2_cpu(i, j)) );
        }
      }

    for (uword i = 0; i < m2_cpu.n_rows; ++i)
      {
      REQUIRE( found[i] == true );
      }

    // Now check the one where we shuffled columns.
    found.clear();
    arma::Mat<eT> m3_cpu(m3);
    found.resize(m3_cpu.n_cols, false);
    for (uword i = 0; i < m3_cpu.n_cols; ++i)
      {
      // Map the first element of the column back to its original column.
      const uword val = m3_cpu(0, i);
      const uword orig_col = (val / rows);
      found[orig_col] = true;

      for (uword j = 1; j < m3_cpu.n_rows; ++j)
        {
        REQUIRE( m_cpu(j, orig_col) == Approx(m3_cpu(j, i)) );
        }
      }

    for (uword i = 0; i < m3_cpu.n_cols; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }



// Shuffle the columns and rows of a matrix of varying sizes that are powers of 2 plus 1.
TEMPLATE_TEST_CASE("shuffle_mat_gt_pow2_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (uword p = 7; p < 11; ++p)
    {
    const uword row_p = p + 1;
    const uword col_p = p - 5;
    const uword rows = std::pow(row_p, 2) + 1;
    const uword cols = std::pow(col_p, 2) + 1;
    arma::Mat<eT> m_cpu(rows, cols);
    for (uword i = 0; i < m_cpu.n_elem; ++i)
      {
      m_cpu[i] = i;
      }

    Mat<eT> m(m_cpu);
    Mat<eT> m2 = shuffle(m, 0);
    Mat<eT> m3 = shuffle(m, 1);

    REQUIRE( m.n_rows == m2.n_rows );
    REQUIRE( m.n_cols == m2.n_cols );
    REQUIRE( m.n_rows == m3.n_rows );
    REQUIRE( m.n_cols == m3.n_cols );

    // First check the one where we shuffled rows.
    std::vector<bool> found(m.n_rows, false);
    arma::Mat<eT> m2_cpu(m2);
    for (uword i = 0; i < m2_cpu.n_rows; ++i)
      {
      // Map the first element of the row back to its original row.
      const uword orig_row = (uword) m2_cpu(i, 0);
      found[orig_row] = true;

      for (uword j = 1; j < m2_cpu.n_cols; ++j)
        {
        REQUIRE( m_cpu(orig_row, j) == Approx(m2_cpu(i, j)) );
        }
      }

    for (uword i = 0; i < m2_cpu.n_rows; ++i)
      {
      REQUIRE( found[i] == true );
      }

    // Now check the one where we shuffled columns.
    found.clear();
    arma::Mat<eT> m3_cpu(m3);
    found.resize(m3_cpu.n_cols, false);
    for (uword i = 0; i < m3_cpu.n_cols; ++i)
      {
      // Map the first element of the column back to its original column.
      const uword val = m3_cpu(0, i);
      const uword orig_col = (val / rows);
      found[orig_col] = true;

      for (uword j = 1; j < m3_cpu.n_rows; ++j)
        {
        REQUIRE( m_cpu(j, orig_col) == Approx(m3_cpu(j, i)) );
        }
      }

    for (uword i = 0; i < m3_cpu.n_cols; ++i)
      {
      REQUIRE( found[i] == true );
      }
    }
  }



// Shuffle a subview of a vector.
TEMPLATE_TEST_CASE("shuffle_vec_subview_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c;
  c.randu(2097);
  c.subvec(15, 15 + 2045 - 1) = linspace<Col<eT>>(0, 2044, 2045);

  Col<eT> c2 = shuffle(c.subvec(15, 15 + 2045 - 1));

  REQUIRE( c2.n_elem == 2045 );

  std::vector<bool> found(2045, false);
  for (uword i = 0; i < 2045; ++i)
    {
    found[(uword) c2[i]] = true;
    }

  for (uword i = 0; i < 2045; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }


// Shuffle a subview of a vector, with an overlapping output.
TEMPLATE_TEST_CASE("shuffle_vec_subview_alias_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> c;
  c.randu(2097);
  c.subvec(15, 15 + 2045 - 1) = linspace<Col<eT>>(0, 2044, 2045);

  c.subvec(5, 5 + 2045 - 1) = shuffle(c.subvec(15, 15 + 2045 - 1));

  std::vector<bool> found(2045, false);
  for (uword i = 0; i < 2045; ++i)
    {
    found[(uword) c[i + 5]] = true;
    }

  for (uword i = 0; i < 2045; ++i)
    {
    REQUIRE( found[i] == true );
    }

  // Make sure we didn't go over the end.
  REQUIRE( (uword) c[2050] == 2035 );
  }


// Shuffle a subview of the rows of a matrix.
TEMPLATE_TEST_CASE("shuffle_subview_rowwise_test_1", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(2280, 10);
  m.fill(5000);
  for (uword i = 100; i < 2200; ++i)
    m.row(i).fill(i - 100);

  Mat<eT> m2 = shuffle(m.rows(100, 2199), 0);

  REQUIRE( m2.n_rows == 2100 );
  REQUIRE( m2.n_cols == m.n_cols );

  arma::Mat<eT> m2_cpu(m2);
  std::vector<bool> found(m2.n_rows, false);
  for (uword i = 0; i < m2.n_rows; ++i)
    {
    const uword val = (uword) m2_cpu(i, 0);
    REQUIRE( val < m2.n_rows );
    found[val] = true;

    for (uword j = 1; j < m2_cpu.n_cols; ++j)
      {
      REQUIRE( ((eT) m2_cpu(i, j)) == ((eT) m2_cpu(i, 0)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle a full subview of the rows of a matrix.
TEMPLATE_TEST_CASE("shuffle_subview_rowwise_test_2", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(2280, 10);
  m.fill(5000);
  for (uword i = 100; i < 2200; ++i)
    m.row(i).fill(i - 100);
  m.col(0).fill(5000);
  m.col(9).fill(5000);

  Mat<eT> m2 = shuffle(m.submat(100, 1, 2199, 8), 0);

  REQUIRE( m2.n_rows == 2100 );
  REQUIRE( m2.n_cols == 8 );

  arma::Mat<eT> m2_cpu(m2);
  std::vector<bool> found(m2.n_rows, false);
  for (uword i = 0; i < m2.n_rows; ++i)
    {
    const uword val = (uword) m2_cpu(i, 0);
    REQUIRE( val < m2.n_rows );
    found[val] = true;

    for (uword j = 1; j < m2_cpu.n_cols; ++j)
      {
      REQUIRE( ((eT) m2_cpu(i, j)) == ((eT) m2_cpu(i, 0)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle a subview of the columns of a matrix.
TEMPLATE_TEST_CASE("shuffle_subview_colwise_test_1", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(10, 2280);
  m.fill(5000);
  for (uword i = 100; i < 2200; ++i)
    m.col(i).fill(i - 100);

  Mat<eT> m2 = shuffle(m.cols(100, 2199), 1);

  REQUIRE( m2.n_rows == m.n_rows );
  REQUIRE( m2.n_cols == 2100 );

  arma::Mat<eT> m2_cpu(m2);
  std::vector<bool> found(m2.n_cols, false);
  for (uword i = 0; i < m2.n_cols; ++i)
    {
    const uword val = (uword) m2_cpu(0, i);
    REQUIRE( val < m2.n_cols );
    found[val] = true;

    for (uword j = 1; j < m2_cpu.n_rows; ++j)
      {
      REQUIRE( ((eT) m2_cpu(j, i)) == ((eT) m2_cpu(0, i)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle a full subview of the columns of a matrix.
TEMPLATE_TEST_CASE("shuffle_subview_colwise_test_2", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(10, 2280);
  m.fill(5000);
  for (uword i = 100; i < 2200; ++i)
    m.col(i).fill(i - 100);
  m.row(0).fill(5000);
  m.row(9).fill(5000);

  Mat<eT> m2 = shuffle(m.submat(1, 100, 8, 2199), 1);

  REQUIRE( m2.n_rows == 8 );
  REQUIRE( m2.n_cols == 2100 );

  arma::Mat<eT> m2_cpu(m2);
  std::vector<bool> found(m2.n_cols, false);
  for (uword i = 0; i < m2.n_cols; ++i)
    {
    const uword val = (uword) m2_cpu(0, i);
    REQUIRE( val < m2.n_cols );
    found[val] = true;

    for (uword j = 1; j < m2_cpu.n_rows; ++j)
      {
      REQUIRE( ((eT) m2_cpu(j, i)) == ((eT) m2_cpu(0, i)) );
      }
    }

  for (uword i = 0; i < found.size(); ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle into a subview of a matrix.
TEMPLATE_TEST_CASE("shuffle_into_subview_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  const uword rows = 77;
  const uword cols = 1025;

  arma::Mat<eT> m_cpu(rows, cols);
  for (uword i = 0; i < m_cpu.n_elem; ++i)
    {
    m_cpu[i] = i;
    }

  Mat<eT> m(m_cpu);
  Mat<eT> m2(rows + 10, cols + 10);
  m2.fill(5000);
  Mat<eT> m3(rows + 10, cols + 10);
  m3.fill(5000);

  m2.submat(3, 4, 3 + rows - 1, 4 + cols - 1) = shuffle(m, 0);
  m3.submat(4, 5, 4 + rows - 1, 5 + cols - 1) = shuffle(m, 1);

  // First check m2.
  arma::Mat<eT> m2_cpu(m2);
  std::vector<bool> found(rows, false);
  for (uword r = 0; r < rows; ++r)
    {
    const uword orig_row = (uword) m2_cpu(r + 3, 4);
    found[orig_row] = true;

    for (uword j = 1; j < cols; ++j)
      {
      REQUIRE( (eT) m_cpu(orig_row, j) == Approx((eT) m2_cpu(r + 3, j + 4)) );
      }
    }

  for (uword i = 0; i < rows; ++i)
    {
    REQUIRE( found[i] == true );
    }

  // Now check m3.
  found.clear();
  found.resize(cols, false);
  arma::Mat<eT> m3_cpu(m3);
  for (uword c = 0; c < cols; ++c)
    {
    const uword val = (uword) m3_cpu(4, c + 5);
    const uword orig_col = val / rows;
    found[orig_col] = true;

    for (uword j = 1; j < rows; ++j)
      {
      REQUIRE( (eT) m_cpu(j, orig_col) == Approx((eT) m3_cpu(j + 4, c + 5)) );
      }
    }

  for (uword i = 0; i < cols; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle where the input is the output.
TEMPLATE_TEST_CASE("shuffle_alias_test_1", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(500, 17);
  for (uword i = 0; i < 500; ++i)
    m.row(i).fill(i);

  m = shuffle(m, 0);

  std::vector<bool> found(500, false);
  arma::Mat<eT> m_cpu(m);
  for (uword i = 0; i < 500; ++i)
    {
    found[(uword) m_cpu(i, 0)] = true;

    for (uword j = 1; j < 17; ++j)
      {
      REQUIRE( (eT) m_cpu(i, 0) == Approx((eT) m_cpu(i, j)) );
      }
    }

  for (uword i = 0; i < 500; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle where the input is a subview of the output.
TEMPLATE_TEST_CASE("shuffle_alias_test_2", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(17, 500);
  for (uword i = 0; i < 500; ++i)
    m.col(i).fill(i);

  m = shuffle(m.submat(3, 0, 14, 255), 1);

  REQUIRE( m.n_rows == 12 );
  REQUIRE( m.n_cols == 256 );

  std::vector<bool> found(256, false);
  arma::Mat<eT> m_cpu(m);
  for (uword i = 0; i < 256; ++i)
    {
    found[(uword) m_cpu(0, i)] = true;

    for (uword j = 1; j < 12; ++j)
      {
      REQUIRE( (eT) m_cpu(0, i) == Approx((eT) m_cpu(j, i)) );
      }
    }

  for (uword i = 0; i < 256; ++i)
    {
    REQUIRE( found[i] == true );
    }
  }



// Shuffle where the input and output are the same subview of a matrix.
TEMPLATE_TEST_CASE("shuffle_subview_inplace_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(17, 500);
  for (uword i = 0; i < 500; ++i)
    m.col(i).fill(i);

  m.submat(3, 0, 14, 255) = shuffle(m.submat(3, 0, 14, 255), 1);

  // First check the shuffled region.
  std::vector<bool> found(256, false);
  arma::Mat<eT> m_cpu(m);
  for (uword i = 0; i < 256; ++i)
    {
    found[(uword) m_cpu(3, i)] = true;

    for (uword j = 1; j < 12; ++j)
      {
      REQUIRE( (eT) m_cpu(3, i) == Approx((eT) m_cpu(3 + j, i)) );
      }
    }

  for (uword i = 0; i < 256; ++i)
    {
    REQUIRE( found[i] == true );
    }

  // Now check the parts of the matrix that should be untouched.
  for (uword i = 0; i < 256; ++i)
    {
    REQUIRE( (eT) m_cpu(0,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(1,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(2,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(15, i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(16, i) == Approx((eT) i) );
    }

  for (uword i = 256; i < 500; ++i)
    {
    for (uword j = 0; j < 17; ++j)
      {
      REQUIRE( (eT) m_cpu(j, i) == Approx((eT) i) );
      }
    }
  }



// Shuffle where the input and output are different subviews of the same matrix.
TEMPLATE_TEST_CASE("shuffle_subview_alias_test", "[shuffle]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> m(17, 500);
  for (uword i = 0; i < 500; ++i)
    m.col(i).fill(i);

  m.submat(3, 10, 14, 265) = shuffle(m.submat(5, 20, 16, 275), 1);

  // First check the shuffled region.
  std::vector<bool> found(256, false);
  arma::Mat<eT> m_cpu(m);
  for (uword i = 0; i < 256; ++i)
    {
    found[(uword) m_cpu(3, 10 + i) - 20] = true;

    for (uword j = 1; j < 12; ++j)
      {
      REQUIRE( (eT) m_cpu(3, 10 + i) == Approx((eT) m_cpu(3 + j, 10 + i)) );
      }
    }

  for (uword i = 0; i < 256; ++i)
    {
    REQUIRE( found[i] == true );
    }

  // Now check the parts of the matrix that should be untouched.
  for (uword i = 0; i < 10; ++i)
    {
    for (uword j = 0; j < 17; ++j)
      {
      REQUIRE( (eT) m_cpu(j, i) == Approx((eT) i) );
      }
    }

  for (uword i = 10; i < 266; ++i)
    {
    REQUIRE( (eT) m_cpu(0,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(1,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(2,  i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(15, i) == Approx((eT) i) );
    REQUIRE( (eT) m_cpu(16, i) == Approx((eT) i) );
    }

  for (uword i = 266; i < 500; ++i)
    {
    for (uword j = 0; j < 17; ++j)
      {
      REQUIRE( (eT) m_cpu(j, i) == Approx((eT) i) );
      }
    }
  }
