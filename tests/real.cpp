// Copyright 2024 Ryan Curtin (https://www.ratml.org)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

// NOTE: complex matrices are not currently "officially" supported, but a very
// simple version of real() and imag() are available (but again "hidden").  They
// probably only work on the CUDA backend.

TEMPLATE_TEST_CASE("real_1", "[real]", float, double)
  {
  typedef TestType eT;
  typedef std::complex<eT> cx_eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  arma::Mat<cx_eT> cx_cpu(50, 50, arma::fill::randu);

  Mat<cx_eT> cx(cx_cpu);

  arma::Mat<eT> real_cpu = real(cx_cpu);
  Mat<eT> real_gpu = real(cx);

  REQUIRE( real_gpu.n_rows == cx.n_rows );
  REQUIRE( real_gpu.n_cols == cx.n_cols );

  arma::Mat<eT> real_gpu_cpu(real_gpu);

  REQUIRE( arma::approx_equal(real_gpu_cpu, real_cpu, "both", 1e-5, 1e-5) );
  }
